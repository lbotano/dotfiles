filetype plugin on

" Plugged
call plug#begin('~/.config/nvim/plugged')
  Plug 'preservim/nerdtree'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'm-pilia/vim-ccls'
  Plug 'cdelledonne/vim-cmake'
  Plug 'yuezk/vim-js'
  Plug 'maxmellon/vim-jsx-pretty'
  Plug 'lervag/vimtex'
  Plug 'mattn/emmet-vim'
  Plug 'digitaltoad/vim-pug'
  Plug 'ekalinin/Dockerfile.vim'
call plug#end()

" Set clang_complete library path
" let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'

" Remap splits navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Remap splits resizing
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize-3<CR>

" Disable pipes in split
set fillchars+=vert:\ 

" Remap terminal bindings
tnoremap <Esc> <C-\><C-n>

" Enable clipboard
set clipboard=unnamedplus

" Set line numeration
set number relativenumber
set nu rnu

" Set tab width
set tabstop=2
set softtabstop=0
set expandtab
set shiftwidth=2
set smarttab

" Toggle NERDTree key binding
nmap <F6> :NERDTreeToggle<CR>

" Open NERDTree on startup
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

" Enable mouse controls
set mouse=a

" Close NERDTree when it's the last pane
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" VimTex config
map <buffer> K <Plug>(vimtex-doc-package)
let g:vimtex_view_general_viewer = 'zathura'
" autocmd BufWritePost *.tex :VimtexCompile

" coc window colors
hi CocInfoFloat guifg=#000000 guibg=#ffffff
hi CocHintFloat guifg=#000000 guibg=#55ff55
hi CocWarningFloat guifg=#000000 guibg=#ffff55
hi CocErrorFloat guifg=#000000 guibg=#ff5555
