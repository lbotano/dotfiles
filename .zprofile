#!/bin/sh

export BROWSER="brave"
export TERMINAL="/usr/local/bin/st"
export PATH="$PATH:$HOME/.yarn/bin:/opt/devkitpro/tools/bin/:${$(find $HOME/.local/bin -type d -printf %p:)%%:}"

# History
export HISTFILE="$HOME/.cache/zsh_history"
export HISTSIZE=10000000
export SAVEHIST=10000000

# Make Java run swing applications with proper font rendering
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

# Make QT use kvantum
export QT_STYLE_OVERRIDE=kvantum

export WINEPREFIX=~/.wine setup_dxvk install

wmname LG3D

# Devkitpro
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=/opt/devkitpro/devkitARM
export DEVKITPPC=/opt/devkitpro/devkitPPC

# Icons for lf
LF_ICONS=$(sed ~/.config/diricons \
            -e '/^[ \t]*#/d'       \
            -e '/^[ \t]*$/d'       \
            -e 's/[ \t]\+/=/g'     \
            -e 's/$/ /')
LF_ICONS=${LF_ICONS//$'\n'/:}
export LF_ICONS
