# Start DWM when logging from TTY1
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
fi

autoload -Uz compinit promptinit colors
compinit
promptinit
colors

# Set theme
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
print -n '\e]2;lbotano\a'

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
# Not supported in the "fish" shell.
(cat ~/.cache/wal/sequences &)

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh

# Preferred editor for local and remote sessions
export EDITOR='vim'

# aliases
alias ls="ls --color=auto"
alias l="ls -l"
alias la="ls -a"
alias lla="ls -la"
alias lt="ls --tree"

alias vim="nvim"
alias v="vim"
alias sv="sudo vim"
alias browser="$BROWSER"
alias irc="irssi"

alias g="git"
alias gtree="git log --graph --oneline --all --decorate --branches"
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

alias newsboat="newsboat --config-file=~/.config/newsboat/"

# Fix something about Java Swing
wmname LG3D

# Enable vim mode
bindkey -v
export KEYTIMEOUT=1

# Make insert mode delete backwards as intended
bindkey "^?" backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Make autocompletion work with sudo
zstyle ':completion::complete:*' gain-privileges 1

# Make autocompletion case insensitive
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# Update executable autocompletion automatically
zstyle ':completion:*' rehash true

# Make autocompletion with only one tab press
unsetopt listambiguous

# Config keybind
typeset -g -A key

key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}" 
key[Left]="${terminfo[kcub1]}" 
key[Right]="${terminfo[kcuf1]}" 
key[Delete]="${terminfo[kdch1]}" 
key[Home]="${terminfo[khome]}" 
key[End]="${terminfo[kend]}" 

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
  autoload -Uz add-zle-hook-widget
  function zle_application_mode_start { echoti smkx }
  function zle_application_mode_stop { echoti rmkx }
  add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
  add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

[[ -n "${key[Left]}"  ]] && bindkey -- "${key[Left]}" backward-char
[[ -n "${key[Right]}"  ]] && bindkey -- "${key[Right]}" forward-char
[[ -n "${key[Delete]}"  ]] && bindkey -- "${key[Delete]}" delete-char
[[ -n "${key[Home]}"  ]] && bindkey -- "${key[Home]}" beginning-of-line
[[ -n "${key[End]}"  ]] && bindkey -- "${key[End]}" end-of-line

# Search story (with up arrow)
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"    ]] && bindkey -- "${key[Up]}" up-line-or-beginning-search
[[ -n "${key[Down]}"  ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

